﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public NumBanderes Banderes;
    private int endGame;
    private int progress;
    public int time;
    // Start is called before the first frame update
    void Start()
    {
        this.progress = 0;
        this.endGame = Banderes.cantBaneres;
        Events.actual.agafaBandera += this.AgafaBandera;
        print(endGame);
    }

    private void AgafaBandera()
    {
        this.progress++;
        if (progress == endGame)
        {
            print("Se Acabo");
            SceneManager.LoadScene("GameOver");
        }
    }
}

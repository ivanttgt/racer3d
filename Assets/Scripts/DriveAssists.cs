﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriveAssists : MonoBehaviour
{
    public CarActions actions;
    private float ResetSteringAngle;
    private float turnNerf;
    private bool ABSactive;
    private float ResetTorque;
    private Vector3 ResetPos;
    private void Awake()
    {
        this.ResetSteringAngle = this.GetComponent<Movimen>().maxSteeringAngle;
        this.ABSactive = false;
        this.ResetTorque = this.GetComponent<Movimen>().maxMotorTorque;
        this.ResetPos = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        resetOnOverturn();
        resetOnFallOff();
        TurnAssist();
        ABSAssist();
    }

    private void resetOnFallOff()
    {
        if (this.transform.position.y < -15)
        {
            this.transform.position = this.ResetPos;
            this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            this.transform.rotation = Quaternion.Euler(0 , 0, 0);
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
    }

    private void ABSAssist()
    {
        if (!ABSactive && Input.GetAxis("Vertical") < -0.8 && !this.actions.isReversing)
        {
            this.ABSactive = true;
            StartCoroutine(this.ABS());
        } else if (Input.GetAxis("Vertical") > -0.8)
        {
            this.ABSactive = false;
            StopCoroutine(this.ABS());
            this.GetComponent<Movimen>().maxMotorTorque = this.ResetTorque;
        }
    }


    /*AQUI ES POT AFEIGIR MES COSA*/
    private void TurnAssist()
    {
        this.turnNerf = 1 - (0.2f * Input.GetAxis("Vertical"));
        if (this.actions.GasForward)
        {
            this.GetComponent<Movimen>().maxSteeringAngle = this.ResetSteringAngle * this.turnNerf;
        } else if (!this.actions.GasForward && this.GetComponent<Movimen>().maxSteeringAngle < this.ResetSteringAngle)
        {
            this.GetComponent<Movimen>().maxSteeringAngle = this.ResetSteringAngle;
        }
    }

    private void resetOnOverturn()
    {
        if (this.transform.rotation.eulerAngles.z > 80 && this.transform.rotation.eulerAngles.z < 280)
        {
            StartCoroutine(ResetCar());
        }
        else
        {
            StopCoroutine(ResetCar());
        }
    }

    private IEnumerator ABS()
    {
        yield return new WaitForSeconds(0.1f);
        this.GetComponent<Movimen>().maxMotorTorque = 0;
        yield return new WaitForSeconds(0.1f);
        this.GetComponent<Movimen>().maxMotorTorque = this.ResetTorque;
        this.ABSactive = false;
    }

    private IEnumerator ResetCar()
    {
        yield return new WaitForSeconds(2);
        if (this.transform.rotation.eulerAngles.z > 80 && this.transform.rotation.eulerAngles.z < 280)
        {
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y, 0);
            this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
        
    }
}

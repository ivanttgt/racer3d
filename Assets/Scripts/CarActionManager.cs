﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarActionManager : MonoBehaviour
{
    public CarActions actions;
    private float zvel;
    public NumBanderes band;
    public Material mat;
    private float updatingColor;
    // Start is called before the first frame update
    void Start()
    {
        this.mat.color = new Color(0,0,1);
        band.lastHittedFlag = new Vector3(0,-50,0);
        this.updatingColor = 1f/(float)band.cantBaneres;
        Events.actual.agafaBandera += this.UpdateColor;
        print(updatingColor);
    }

    // Update is called once per frame
    void Update()
    {
        SetBreak_Reverse();
        SetGasActions();
        SetPos();
    }

    private void SetPos()
    {
        this.actions.pos = this.transform.position;
    }

    private void UpdateColor()
    {
        this.mat.color = new Color(this.mat.color.r + this.updatingColor, this.mat.color.g, this.mat.color.b - this.updatingColor);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.tag == "Banderes")
        {
            band.lastHittedFlag = collision.transform.position;
            Events.actual.PillantBandera();
        }
    }

    private void SetGasActions()
    {
        if (Input.GetAxis("Vertical") < 0)
        {
            this.actions.GasBackward = true;
            this.actions.GasForward = false;
        }
        else if (Input.GetAxis("Vertical") > 0)
        {
            this.actions.GasBackward = false;
            this.actions.GasForward = true;
        } else
        {
            this.actions.GasBackward = false;
            this.actions.GasForward = false;
        }
    }

    private void SetBreak_Reverse()
    {
        zvel = Vector3.Dot(this.GetComponent<Rigidbody>().velocity, this.transform.forward);
        //print(zvel);


        if (zvel <= -0.2 && Input.GetAxis("Vertical") < 0)
        {
            this.actions.isReversing = true;
            this.actions.isBreaking = false;
        }
        else if (zvel <= -0.2 && Input.GetAxis("Vertical") > 0)
        {
            this.actions.isReversing = false;
            this.actions.isBreaking = true;
        }
        else if (zvel >= 0 && Input.GetAxis("Vertical") < 0)
        {
            this.actions.isReversing = false;
            this.actions.isBreaking = true;
        }
        else
        {
            this.actions.isReversing = false;
            this.actions.isBreaking = false;
        }
    }
}

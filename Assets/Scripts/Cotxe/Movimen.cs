﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public List<AxleInfo> axleInfos; // the information about each individual axle
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxSteeringAngle; // maximum steer angle the wheel can have
    public CarActions actions;

    public void FixedUpdate()
    {
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel, true);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel, false);
        }
    }

    private void ResetCarRotation()
    {
        this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y, 0);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+0.5f, this.transform.position.z);
        this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
     }

    public void ApplyLocalPositionToVisuals(WheelCollider collider, bool isAtLeft)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);
        rotation = rotation * Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z));
        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
        if (isAtLeft)
            //@CROnerd64 use local forward there, local up didn't seem to work properly
            visualWheel.transform.Rotate(0, 0, 180f, Space.Self);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            
            this.ResetCarRotation();
        }
    }

    [System.Serializable]
    public class AxleInfo
    {
        public WheelCollider leftWheel;
        public WheelCollider rightWheel;
        public bool motor; // is this wheel attached to motor?
        public bool steering; // does this wheel apply steer angle?
    }
}

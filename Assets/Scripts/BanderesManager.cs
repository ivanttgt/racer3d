﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanderesManager : MonoBehaviour
{
    public NumBanderes band;
    private void Awake()
    {
        band.cantBaneres = this.transform.GetChildCount();
    }

    internal Vector3 GetLastpos()
    {
        return this.band.lastHittedFlag;
    }

    internal void ResetLastPos()
    {
        this.band.lastHittedFlag = new Vector3(0,-50,0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CarActions : ScriptableObject
{
    public bool isBreaking;
    public bool isReversing;
    public bool GasForward;
    public bool GasBackward;
    public Vector3 pos;
}

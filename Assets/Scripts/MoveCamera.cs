﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject Carro;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //movem la camera
        mainCamera.transform.position = new Vector3(
            //Seguir X
            Mathf.Lerp(mainCamera.transform.position.x, Carro.transform.position.x - Carro.transform.forward.x * 6, Time.deltaTime * 2),
            //Seguir Y
            Mathf.Lerp(mainCamera.transform.position.y, Carro.transform.position.y + 3, Time.deltaTime * 10),
            //Seguit Z
            Mathf.Lerp(mainCamera.transform.position.z, Carro.transform.position.z - Carro.transform.forward.z * 6, Time.deltaTime * 2)
            );
        //rota la camera per a que el seu vector forward apunti cap al punt donat
        Vector3 target = new Vector3(Carro.transform.position.x, Carro.transform.position.y + 2f, Carro.transform.position.z);
        mainCamera.transform.LookAt(target);
    }

}

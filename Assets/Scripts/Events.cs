﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events : MonoBehaviour
{
    public static Events actual;
    public event Action agafaBandera;


    private void Awake()
    {
        actual = this;
    }

    public void PillantBandera()
    {
        if (agafaBandera != null)
        {
            agafaBandera();
        }
    }
}

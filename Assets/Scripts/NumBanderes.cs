﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class NumBanderes : ScriptableObject
{
    public int cantBaneres;
    public Vector3 lastHittedFlag;
}

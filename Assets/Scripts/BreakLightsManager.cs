﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakLightsManager : MonoBehaviour
{
    public CarActions actions;
    private Color red;
    private Color white;

    private void Start()
    {
        this.red.r = 1;
        this.red.g = 0;
        this.red.b = 0;

        this.white.r = 1;
        this.white.g = 1;
        this.white.b = 1;
    }
    void Update()
    {
        if (this.actions.isReversing && !this.actions.isBreaking)
        {
            this.GetComponent<Light>().color = this.white;
            this.GetComponent<Light>().intensity = 1;
        } else if (this.actions.isBreaking)
        {
            this.GetComponent<Light>().color = this.red;
            this.GetComponent<Light>().intensity = 2;
        } else
        {
            this.GetComponent<Light>().intensity = 0;
        }
    }
}

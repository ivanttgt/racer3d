﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirarAPJ : MonoBehaviour
{
    public CarActions actions;
    // Start is called before the first frame update
    void Start()
    {
        Events.actual.agafaBandera += this.BorrarBandera;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = new Vector3(actions.pos.x, this.transform.position.y, actions.pos.z);
        this.transform.LookAt(target);
    }

    private void BorrarBandera()
    {
        if (this.GetComponentInParent<BanderesManager>().GetLastpos() == this.transform.position)
        {
            this.GetComponentInParent<BanderesManager>().ResetLastPos();
            this.gameObject.SetActive(false);
        }
    }
}
